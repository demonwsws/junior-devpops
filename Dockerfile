FROM python:3.8
COPY requirements/production.txt /tmp/requirements/production.txt
RUN pip install --no-cache -r  /tmp/requirements/production.txt

COPY dist /tmp/dist
RUN pip install /tmp/dist/*

COPY run_service.py run_servicer.py

EXPOSE 8085

CMD ["python", "run_servicer.py", "--port", "8085"]
